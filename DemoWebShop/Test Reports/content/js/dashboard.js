/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8582089552238806, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 66, 0, 0.0, 491.5606060606061, 195, 4020, 226.0, 1275.2000000000003, 1902.1499999999996, 4020.0, 4.640371229698376, 161.84338307670674, 8.41575375272446], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 1, 0, 0.0, 252.0, 252, 252, 252.0, 252.0, 252.0, 252.0, 3.968253968253968, 0.48053075396825395, 3.5574776785714284], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 1, 0, 0.0, 251.0, 251, 251, 251.0, 251.0, 251.0, 251.0, 3.9840637450199203, 0.48244521912350596, 3.5327440239043826], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 1, 0, 0.0, 251.0, 251, 251, 251.0, 251.0, 251.0, 251.0, 3.9840637450199203, 0.47855453187250996, 3.5521974601593627], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 1, 0, 0.0, 255.0, 255, 255, 255.0, 255.0, 255.0, 255.0, 3.9215686274509802, 0.47870710784313725, 3.4466911764705883], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 1, 0, 0.0, 1313.0, 1313, 1313, 1313.0, 1313.0, 1313.0, 1313.0, 0.7616146230007617, 327.8118752380046, 0.8464037509520184], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 1, 0, 0.0, 226.0, 226, 226, 226.0, 226.0, 226.0, 226.0, 4.424778761061947, 13.248409845132743, 5.029728982300885], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 2, 0, 0.0, 3282.0, 2544, 4020, 3282.0, 4020.0, 4020.0, 4020.0, 0.21094821221390148, 117.38876601887986, 4.0130631328446364], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 1, 0, 0.0, 1962.0, 1962, 1962, 1962.0, 1962.0, 1962.0, 1962.0, 0.5096839959225281, 19.361023509174313, 11.111509301732927], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 1, 0, 0.0, 218.0, 218, 218, 218.0, 218.0, 218.0, 218.0, 4.587155963302752, 17.649799311926607, 5.272541571100917], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 1, 0, 0.0, 197.0, 197, 197, 197.0, 197.0, 197.0, 197.0, 5.076142131979695, 19.536207170050762, 5.745360088832487], "isController": false}, {"data": ["Test", 1, 0, 0.0, 8526.0, 8526, 8526, 8526.0, 8526.0, 8526.0, 8526.0, 0.1172882946281961, 134.99287107084214, 7.019544078407225], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 1, 0, 0.0, 213.0, 213, 213, 213.0, 213.0, 213.0, 213.0, 4.694835680751174, 18.068698650234744, 5.377970950704225], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 1, 0, 0.0, 211.0, 211, 211, 211.0, 211.0, 211.0, 211.0, 4.739336492890995, 18.304761552132703, 5.401177428909953], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 1, 0, 0.0, 248.0, 248, 248, 248.0, 248.0, 248.0, 248.0, 4.032258064516129, 0.49221900201612906, 3.547914566532258], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 1, 0, 0.0, 195.0, 195, 195, 195.0, 195.0, 195.0, 195.0, 5.128205128205129, 38.14102564102564, 5.799278846153846], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 2, 0, 0.0, 672.0, 218, 1126, 672.0, 1126.0, 1126.0, 1126.0, 0.3257859586251832, 4.081868993321388, 0.31608237294347613], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 2, 0, 0.0, 833.5, 408, 1259, 833.5, 1259.0, 1259.0, 1259.0, 0.3389256058295204, 21.36290459244196, 0.3078132943568887], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 2, 0, 0.0, 310.0, 215, 405, 310.0, 405.0, 405.0, 405.0, 0.32599837000814996, 15.055298746943766, 0.3185169621026895], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 2, 0, 0.0, 650.5, 206, 1095, 650.5, 1095.0, 1095.0, 1095.0, 0.32647730982696704, 4.371416401403852, 0.32137610186092064], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 2, 0, 0.0, 558.0, 223, 893, 558.0, 893.0, 893.0, 893.0, 0.3255738238645613, 1.3328178414455478, 0.3077690053719681], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 2, 0, 0.0, 653.5, 215, 1092, 653.5, 1092.0, 1092.0, 1092.0, 0.32599837000814996, 3.904499032192339, 0.3045092196414018], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 2, 0, 0.0, 206.5, 206, 207, 206.5, 207.0, 207.0, 207.0, 0.3676470588235294, 1.3677260454963234, 0.35400390625], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 2, 0, 0.0, 1002.0, 213, 1791, 1002.0, 1791.0, 1791.0, 1791.0, 0.34934497816593885, 38.98812772925764, 0.33638100436681223], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 2, 0, 0.0, 1066.0, 376, 1756, 1066.0, 1756.0, 1756.0, 1756.0, 0.27348557363599074, 3.2046419902912624, 0.1805432107206345], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 1, 0, 0.0, 226.0, 226, 226, 226.0, 226.0, 226.0, 226.0, 4.424778761061947, 0.5358130530973452, 3.9192132190265485], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 1, 0, 0.0, 210.0, 210, 210, 210.0, 210.0, 210.0, 210.0, 4.761904761904763, 0.5719866071428572, 4.241071428571429], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 1, 0, 0.0, 226.0, 226, 226, 226.0, 226.0, 226.0, 226.0, 4.424778761061947, 0.5358130530973452, 3.837112831858407], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 1, 0, 0.0, 210.0, 210, 210, 210.0, 210.0, 210.0, 210.0, 4.761904761904763, 0.5766369047619048, 4.110863095238096], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 1, 0, 0.0, 654.0, 654, 654, 654.0, 654.0, 654.0, 654.0, 1.529051987767584, 52.68957855504587, 1.1363364870030581], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 1, 0, 0.0, 341.0, 341, 341, 341.0, 341.0, 341.0, 341.0, 2.932551319648094, 1.9788993768328444, 3.0413764662756595], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 1, 0, 0.0, 281.0, 281, 281, 281.0, 281.0, 281.0, 281.0, 3.558718861209964, 0.4274633007117437, 3.0826012010676154], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 1, 0, 0.0, 281.0, 281, 281, 281.0, 281.0, 281.0, 281.0, 3.558718861209964, 0.43093861209964407, 3.0443727758007113], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 1, 0, 0.0, 210.0, 210, 210, 210.0, 210.0, 210.0, 210.0, 4.761904761904763, 0.5719866071428572, 4.059709821428571], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 1, 0, 0.0, 226.0, 226, 226, 226.0, 226.0, 226.0, 226.0, 4.424778761061947, 0.5358130530973452, 3.966745022123894], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 2, 0, 0.0, 209.5, 205, 214, 209.5, 214.0, 214.0, 214.0, 0.3811701924909472, 0.6584863969887554, 0.3670252048789785], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 1, 0, 0.0, 266.0, 266, 266, 266.0, 266.0, 266.0, 266.0, 3.7593984962406015, 0.4552396616541353, 3.194020206766917], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 1, 0, 0.0, 266.0, 266, 266, 266.0, 266.0, 266.0, 266.0, 3.7593984962406015, 0.4552396616541353, 3.234404370300752], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 1, 0, 0.0, 281.0, 281, 281, 281.0, 281.0, 281.0, 281.0, 3.558718861209964, 0.43093861209964407, 3.072175266903914], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 1, 0, 0.0, 206.0, 206, 206, 206.0, 206.0, 206.0, 206.0, 4.854368932038835, 0.5783525485436893, 5.835672026699029], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 1, 0, 0.0, 394.0, 394, 394, 394.0, 394.0, 394.0, 394.0, 2.5380710659898473, 138.18359375, 2.8206297588832485], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 1, 0, 0.0, 206.0, 206, 206, 206.0, 206.0, 206.0, 206.0, 4.854368932038835, 0.5878337378640777, 4.290238167475728], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 1, 0, 0.0, 213.0, 213, 213, 213.0, 213.0, 213.0, 213.0, 4.694835680751174, 136.74167400234742, 5.322953345070423], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 1, 0, 0.0, 216.0, 216, 216, 216.0, 216.0, 216.0, 216.0, 4.62962962962963, 0.8951822916666666, 3.978587962962963], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 1, 0, 0.0, 206.0, 206, 206, 206.0, 206.0, 206.0, 206.0, 4.854368932038835, 0.5830931432038835, 5.68871359223301], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 1, 0, 0.0, 206.0, 206, 206, 206.0, 206.0, 206.0, 206.0, 4.854368932038835, 0.5878337378640777, 4.16698270631068], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 2, 0, 0.0, 238.0, 206, 270, 238.0, 270.0, 270.0, 270.0, 0.3968253968253968, 1.4706566220238095, 0.3824869791666667], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 1, 0, 0.0, 203.0, 203, 203, 203.0, 203.0, 203.0, 203.0, 4.926108374384237, 0.5868996305418719, 4.26704895320197], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 1, 0, 0.0, 200.0, 200, 200, 200.0, 200.0, 200.0, 200.0, 5.0, 0.60546875, 5.9375], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 1, 0, 0.0, 209.0, 209, 209, 209.0, 209.0, 209.0, 209.0, 4.784688995215311, 0.5747233851674641, 3.999700956937799], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 2, 0, 0.0, 200.0, 198, 202, 200.0, 202.0, 202.0, 202.0, 0.38218994840435694, 0.7453450458627938, 0.36782050210204476], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 1, 0, 0.0, 206.0, 206, 206, 206.0, 206.0, 206.0, 206.0, 4.854368932038835, 0.5878337378640777, 4.276016383495146], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 2, 0, 0.0, 239.5, 207, 272, 239.5, 272.0, 272.0, 272.0, 0.3966679888932963, 0.30137470249900833, 0.37826786245537486], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 1, 0, 0.0, 266.0, 266, 266, 266.0, 266.0, 266.0, 266.0, 3.7593984962406015, 0.4552396616541353, 3.201362781954887], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 66, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
